#!/usr/bin/env bash
# ########################################################################################
# Zmeny: Testovane na SGS 8/9/21 s default Samsung Messengerom
# ----------------------------------------------------------------------------------------
# 2021-09-10 - MB: dopleneny typ, sms brana 4ky
#		+ pridany typ testu: test_type="Typ: PCR"
#		+ pridany neaktivny riadok : #service_center="+421950900050" # 4ka
#		+ zmena pridana premenna test_type: sms_body2="NCZI: ${meno_rok}, ${covid_pass_id}, ${test_type}, odber zo dna ${test_date} je NEGATIVNY. V pripade zmeny zdravotneho stavu, volajte vseobecneho lekara."
# 2021-09-13 - pridany typ "AG"
# 2021-09-18 - zmena: nczi_sms_sender="NCZI"
# 2021-10-02 - datum sa generuje - dnesny - ale je mozne generovat aj do buducnosti, potom treba hodnoty nastavit manualne
##########################################################################################
# datum a cas testu: - datum testu a odoslania SMS
##########################################################################################
rok=$(date +%Y) ;
mes=$(date +%m) ;
den=$(date +%d) ; # ak chcem iny den, napr. zajtra pozajtra tak to tu treba zmenit
# hod a min, bolo len na generovanie SMS na rezervaciou testovania ale uz negeneruje, len vysledok testu
hod=07 ; min=30 ;
##########################################################################################
# Meno Preszvisko, Rok narodenia, COVID PASS ID
##########################################################################################
# Zadaj TYP testu
test_type="Typ: PCR"
#test_type="Typ: AG"
# Zadaj "Meno PRIEZVISKO, ROK" presne v takomto formate, Meno: prve pismeno velke a zbytok male, Priezvisko velkymi, rok v YYYY
meno_rok="Meno PRIEZVISKO, ROK"
# Zadaj nejake alebo svoje COVID_PASS_ID:
covid_pass_id="COVID-19 PASS: XXX-BBB-AAA"
### START: tato fuga ma svoj zmysel .. pre mna :-) #######################################
#
#
#
#
#
#
#
#
#
#
#
#
### END ##################################################################################
##########################################################################################
# NCZI, Nazov MOMky, Adresa, Telefonne cislo, SMS Gateway
##########################################################################################
# NZCi SMS sender number
# nczi_sms_sender="+421940682016"
nczi_sms_sender="NCZI"
# Operator Service center - tu zadaj cislo SMS centra svojho operatora !!! kazdy ma ine !!!
#service_center="+421949909909" # O2
service_center="+421903333000" # TCom
#service_center="+421950900050" # 4ka
# Info o MOMke kde si bol odovzdat biologicky material - porovnaj ci taku info mas v realnej SMS z testu
mom_name="NZCI"
#mom_name="AG TOP Figura"
#mom_address="Kosice - Cermelska cesta (2), Cermelska cesta 1, Kosice unimobunka na parkovisku pred hotelom Lokomotiva 1452/1"
##########################################################################################
datum_testu_ep1="$(date --date "${rok}-${mes}-${den} ${hod}:${min}" '+%s')";
#datum_objednania_na_test_ep=$(date --date @$((${datum_testu_ep1}-86400-612)) "+%s") # den vopred    # EPOCH
#datum_objednania_na_test=$(date --date @${datum_objednania_na_test_ep} "+%d.%m.%Y %T") # den vopred # 21.04.2021 08:30
test_date="$(date --date @${datum_testu_ep1} '+%d.%m.%Y')";             # 21.04.2021
test_time="$(date --date @${datum_testu_ep1} '+%T')";                   # 08:30
test_date_time="$(date --date @${datum_testu_ep1} '+%d.%m.%Y %T')";     # 21.04.2021 08:30

backup_date=$(date "+%s");                      # NOW - nejaky random   # 1619268816553

#sms_date1=${datum_objednania_na_test_ep};       # datum = test_date_time -  24h/1den    # 1618905401000
#date_sent1=${datum_objednania_na_test_ep};      # datum = sms_date1 + 3600              # 1618905421000
#readable_date1=${datum_objednania_na_test};     # datum = sms_date1             # 2021/04/20 09:56:40

sms_date2="$(date --date @$((${datum_testu_ep1}+3600+600)) '+%s')";   # 1618905401000
date_sent2="${sms_date2}";					# datum = sms_date2 + 3600               # 1618905421000
readable_date2="$(date --date @$((${date_sent2}+125)) '+%d.%m.%Y %T')";     # datum = sms_date2             # 2021/04/20 09:56:40
#
sms_count=1;                    # pocer SMS : spocitaj riadky zacinajuce na "^<sms protocol" # najskor 2, jedno je objednanie na test a druje vysledok testu
#
#
sms_body2="NCZI: ${meno_rok}, ${covid_pass_id}, ${test_type}, odber zo dna ${test_date} je NEGATIVNY. V pripade zmeny zdravotneho stavu, volajte vseobecneho lekara."
#
backupset_id=$(uuidgen) 	# musi byt dostupny progra/prikaz uuidgen
xml_file=sms-${meno_rok/ */}-$(date --date @${datum_testu_ep1} "+%Y%m%d%H%M%S").xml
#
echo file=${xml_file}
#cat > ${xml_file} <<EOF
cat <<EOF
<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<!--File Created By SMS Backup & Restore v10.10.001 on 24/04/2021 14:53:45-->
    To view this file in a more readable format, visit https://synctech.com.au/view-backup/
    To restore use: https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore
-->
<?xml-stylesheet type="text/xsl" href="sms.xsl"?>
<smses count="${sms_count}" backup_set="${backupset_id}" backup_date="${backup_date}000" type="full">

<sms protocol="0" address="${nczi_sms_sender}" date="${sms_date2}000" type="1" subject="null" body="${sms_body2}" toa="null" sc_toa="null" service_center="${service_center}" read="0" status="-1" locked="0" date_sent="${date_sent2}000" sub_id="3" readable_date="${readable_date2}" contact_name="${mom_name}" />
</smses>
EOF
#
#cat ${xml_file}
#ls -la ${xml_file}
